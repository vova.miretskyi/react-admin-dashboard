import {
  Contacts,
  Dashboard,
  Team,
  Form,
  Calendar,
  FAQ,
  Invoices,
  Bar,
  Pie,
  Line,
  Geography,
} from "../scenes";

export type AdminRoute = {
  element: JSX.Element;
  path: string;
};

export const PAGES_URLS = {
  DASHBOARD: "/",
  TEAM: "/team",
  CONTACTS: "/contacts",
  INVOICES: "/invoices",
  FORM: "/form",
  BAR: "/bar",
  PIE: "/pie",
  LINE: "/line",
  FAQ: "/faq",
  GEOGRAPHY: "/geography",
  CALENDAR: "/calendar",
} as const;

export const AdminRoutes: AdminRoute[] = [
  {
    element: <Dashboard />,
    path: PAGES_URLS.DASHBOARD,
  },
  {
    element: <Team />,
    path: PAGES_URLS.TEAM,
  },
  {
    element: <Contacts />,
    path: PAGES_URLS.CONTACTS,
  },
  {
    element: <Invoices />,
    path: PAGES_URLS.INVOICES,
  },
  {
    element: <Form />,
    path: PAGES_URLS.FORM,
  },
  {
    element: <FAQ />,
    path: PAGES_URLS.FAQ,
  },
  {
    element: <Calendar />,
    path: PAGES_URLS.CALENDAR,
  },
  {
    element: <Bar />,
    path: PAGES_URLS.BAR,
  },
  {
    element: <Pie />,
    path: PAGES_URLS.PIE,
  },
  {
    element: <Line />,
    path: PAGES_URLS.LINE,
  },

  {
    element: <Geography />,
    path: PAGES_URLS.GEOGRAPHY,
  },
];
