import { Box } from "@mui/material";
import { PieChart } from "../../components";
import { Page } from "../../hocs";

export const Pie = () => {
  return (
    <Page title="Pie Chart" subtitle="Simple Pie Chart">
      <Box height="75vh">
        <PieChart />
      </Box>
    </Page>
  );
};
