import { Box } from "@mui/material";

import { Page } from "../../hocs";
import { GeographyChart } from "../../components";

export const Geography = () => {
  return (
    <Page title="Geography" subtitle="Simple Geography Chart">
      <Box height="75vh">
        <GeographyChart />
      </Box>
    </Page>
  );
};
