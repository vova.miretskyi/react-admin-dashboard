import { Box, useTheme } from "@mui/material";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { GridBaseColDef } from "@mui/x-data-grid/internals";

import { tokens } from "../../theme";
import { ContactModel } from "../../data/mockData";
import { useFetchContacts } from "../../hooks";
import { Page } from "../../hocs";

export const Contacts = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const { contacts, onSortContacts, isFetching } = useFetchContacts();

  const columns: GridBaseColDef<ContactModel>[] = [
    { field: "id", headerName: "ID", flex: 0.5 },
    { field: "registrarId", headerName: "Registrar ID" },
    {
      field: "name",
      headerName: "Name",
      flex: 1,
      cellClassName: "name-column--cell",
    },
    {
      field: "age",
      headerName: "Age",
      type: "number",
      headerAlign: "left",
      align: "left",
    },
    {
      field: "phone",
      headerName: "Phone Number",
      flex: 1,
    },
    {
      field: "email",
      headerName: "Email",
      flex: 1,
    },
    {
      field: "address",
      headerName: "Address",
      flex: 1,
    },
    {
      field: "city",
      headerName: "City",
      flex: 1,
    },
    {
      field: "zipCode",
      headerName: "ZipCode",
      flex: 1,
    },
  ];

  return (
    <Page title="CONTACTS" subtitle="List of Contacts for Future Reference">
      <Box
        m="40px 0 0 0"
        height="75vh"
        sx={{
          "& .MuiDataGrid-root": {
            border: "none",
          },
          "& .MuiDataGrid-cell": {
            borderBottom: "none",
          },
          "& .name-column--cell": {
            color: colors.greenAccent[300],
          },
          "& .MuiDataGrid-columnHeaders": {
            backgroundColor: colors.blueAccent[700],
            borderBottom: "none",
          },
          "& .MuiDataGrid-virtualScroller": {
            backgroundColor: colors.primary[400],
          },
          "& .MuiDataGrid-footerContainer": {
            borderTop: "none",
            backgroundColor: colors.blueAccent[700],
          },
          "& .MuiDataGrid-toolbarContainer .MuiButton-text": {
            color: `${colors.grey[100]} !important`,
          },
        }}
      >
        <DataGrid
          checkboxSelection
          rows={contacts}
          columns={columns}
          slots={{ toolbar: GridToolbar }}
          onSortModelChange={(Model) => onSortContacts(Model)}
          onFilterModelChange={(Model) => console.log("filter changed", Model)}
          slotProps={{
            toolbar: { counter: contacts.length },
          }}
          initialState={{
            pagination: {
              paginationModel: { pageSize: 10 },
            },
          }}
          pageSizeOptions={[10, 15, 25]}
          loading={isFetching}
        />
      </Box>
    </Page>
  );
};
