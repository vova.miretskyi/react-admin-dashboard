import { Box } from "@mui/material";
import { LineChart } from "../../components";
import { Page } from "../../hocs";

export const Line = () => {
  return (
    <Page title="Line Chart" subtitle="Simple Line Chart">
      <Box height="75vh">
        <LineChart />
      </Box>
    </Page>
  );
};
