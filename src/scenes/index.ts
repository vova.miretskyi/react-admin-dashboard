import { Dashboard } from "./dashboard";
import { Team } from "./team";
import { Contacts } from "./contacts";
import { Invoices } from "./invoices";
import { Form } from "./form";
import { FAQ } from "./faq";
import { Bar } from "./bar";
import { Pie } from "./pie";
import { Line } from "./line";
import { Geography } from "./geography";
import { Calendar } from "./calendar";

export {
  Dashboard,
  Team,
  Contacts,
  Invoices,
  Form,
  FAQ,
  Bar,
  Pie,
  Line,
  Geography,
  Calendar,
};
