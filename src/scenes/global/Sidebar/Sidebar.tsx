import { useState } from "react";
import { useLocation } from "react-router-dom";
import { Box, IconButton, Typography, useTheme } from "@mui/material";
import { ProSidebar, Menu, MenuItem } from "react-pro-sidebar";
import "react-pro-sidebar/dist/css/styles.css";

import { tokens } from "../../../theme";
import { PAGE_LABELS, findPageRouteByPath } from "../../../utils";
import { PAGES_URLS } from "../../../routes";

import { Item } from "./Item";

import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import PeopleOutlinedIcon from "@mui/icons-material/PeopleOutlined";
import ContactsOutlinedIcon from "@mui/icons-material/ContactsOutlined";
import ReceiptOutlinedIcon from "@mui/icons-material/ReceiptOutlined";
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined";
import CalendarTodayOutlinedIcon from "@mui/icons-material/CalendarTodayOutlined";
import HelpOutlinedIcon from "@mui/icons-material/HelpOutlined";
import BarChartOutlinedIcon from "@mui/icons-material/BarChartOutlined";
import PieChartOutlineOutlinedIcon from "@mui/icons-material/PieChartOutlineOutlined";
import TimelineOutlinedIcon from "@mui/icons-material/TimelineOutlined";
import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";
import MapOutlinedIcon from "@mui/icons-material/MapOutlined";

export const Sidebar = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [isCollapsed, setIsCollapsed] = useState(false);
  const location = useLocation();

  const route = findPageRouteByPath(location.pathname);

  const onCollapse = (collapsed: boolean) => () => {
    setIsCollapsed(collapsed);
  };

  return (
    <Box
      sx={{
        "& .pro-sidebar-inner": {
          background: `${colors.primary[400]} !important`,
        },

        "& .pro-icon-wrapper": {
          backgroundColor: "transparent !important",
        },

        "& .pro-inner-item": {
          padding: "5px 35px 5px 20px !important",
        },

        "& .pro-inner-item:hover": {
          color: "#868dfb !important",
        },

        "& .pro-menu-item.active": {
          color: "#868dfb !important",
        },
      }}
    >
      <ProSidebar collapsed={isCollapsed}>
        <Menu iconShape="square">
          <MenuItem
            onClick={onCollapse(!isCollapsed)}
            icon={isCollapsed ? <MenuOutlinedIcon /> : undefined}
            style={{
              margin: "10px 0 29px 0",
              color: colors.grey[100],
            }}
          >
            {!isCollapsed && (
              <Box
                display="flex"
                justifyContent="space-between"
                alignItems="center"
                ml="15px"
              >
                <Typography variant="h3" color={colors.grey[100]}>
                  ADMINS
                </Typography>
                <IconButton onClick={onCollapse(!isCollapsed)}>
                  <MenuOutlinedIcon />
                </IconButton>
              </Box>
            )}
          </MenuItem>
          {!isCollapsed && (
            <Box mb="25px">
              <Box display="flex" justifyContent="center" alignItems="center">
                <img
                  alt="profile-user"
                  width="100px"
                  height="100px"
                  src="https://upload.wikimedia.org/wikipedia/en/thumb/2/2d/Bobby_singer.jpg/240px-Bobby_singer.jpg"
                  style={{
                    cursor: "pointer",
                    borderRadius: "50%",
                  }}
                />
              </Box>
              <Box textAlign="center">
                <Typography
                  variant="h2"
                  color={colors.grey[100]}
                  fontWeight="bold"
                  sx={{ m: "10px 0 0 0" }}
                >
                  Bobby Singer
                </Typography>
                <Typography variant="h5" color={colors.greenAccent[500]}>
                  VP Fancy Admin
                </Typography>
              </Box>
            </Box>
          )}
          <Box paddingLeft={isCollapsed ? undefined : "10%"}>
            <Item
              title={PAGE_LABELS.dashboard}
              to={PAGES_URLS.DASHBOARD}
              icon={<HomeOutlinedIcon />}
              selected={!route}
            />
            <Typography
              variant="h6"
              color={colors.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              Data
            </Typography>
            <Item
              title={PAGE_LABELS.team}
              to={PAGES_URLS.TEAM}
              icon={<PeopleOutlinedIcon />}
              selected={PAGES_URLS.TEAM.includes(route)}
            />
            <Item
              title={PAGE_LABELS.contacts}
              to={PAGES_URLS.CONTACTS}
              icon={<ContactsOutlinedIcon />}
              selected={PAGES_URLS.CONTACTS.includes(route)}
            />
            <Item
              title={PAGE_LABELS.invoices}
              to={PAGES_URLS.INVOICES}
              icon={<ReceiptOutlinedIcon />}
              selected={PAGES_URLS.INVOICES.includes(route)}
            />
            <Typography
              variant="h6"
              color={colors.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              Pages
            </Typography>
            <Item
              title={PAGE_LABELS.form}
              to={PAGES_URLS.FORM}
              icon={<PersonOutlinedIcon />}
              selected={PAGES_URLS.FORM.includes(route)}
            />
            <Item
              title={PAGE_LABELS.calendar}
              to={PAGES_URLS.CALENDAR}
              icon={<CalendarTodayOutlinedIcon />}
              selected={PAGES_URLS.CALENDAR.includes(route)}
            />
            <Item
              title={PAGE_LABELS.faq}
              to={PAGES_URLS.FAQ}
              icon={<HelpOutlinedIcon />}
              selected={PAGES_URLS.FAQ.includes(route)}
            />
            <Typography
              variant="h6"
              color={colors.grey[300]}
              sx={{ m: "15px 0 5px 20px" }}
            >
              Charts
            </Typography>
            <Item
              title={PAGE_LABELS.bar}
              to={PAGES_URLS.BAR}
              icon={<BarChartOutlinedIcon />}
              selected={PAGES_URLS.BAR.includes(route)}
            />
            <Item
              title={PAGE_LABELS.pie}
              to={PAGES_URLS.PIE}
              icon={<PieChartOutlineOutlinedIcon />}
              selected={PAGES_URLS.PIE.includes(route)}
            />
            <Item
              title={PAGE_LABELS.line}
              to={PAGES_URLS.LINE}
              icon={<TimelineOutlinedIcon />}
              selected={PAGES_URLS.LINE.includes(route)}
            />
            <Item
              title={PAGE_LABELS.geography}
              to={PAGES_URLS.GEOGRAPHY}
              icon={<MapOutlinedIcon />}
              selected={PAGES_URLS.GEOGRAPHY.includes(route)}
            />
          </Box>
        </Menu>
      </ProSidebar>
    </Box>
  );
};
