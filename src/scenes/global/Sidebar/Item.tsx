import { FC } from "react";
import { Link } from "react-router-dom";
import { Typography, useTheme } from "@mui/material";
import { MenuItem } from "react-pro-sidebar";

import { tokens } from "../../../theme";

type Props = {
  title: string;
  to: string;
  icon: JSX.Element;
  selected: boolean;
};

export const Item: FC<Props> = ({ title, to, icon, selected }) => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  return (
    <MenuItem active={selected} style={{ color: colors.grey[100] }} icon={icon}>
      <Typography>{title}</Typography>
      <Link to={to}></Link>
    </MenuItem>
  );
};
