import { Box } from "@mui/material";
import { BarChart } from "../../components";
import { Page } from "../../hocs";

export const Bar = () => {
  return (
    <Page title="Bar Chart" subtitle="Simple Bar Chart">
      <Box height="75vh">
        <BarChart />
      </Box>
    </Page>
  );
};
