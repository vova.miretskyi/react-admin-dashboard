import { useState } from "react";
import { ContactModel, mockDataContacts } from "../data/mockData";

const fetchData = async (): Promise<ContactModel[]> => {
  return new Promise((resolve, reject) => {
    try {
      setTimeout(() => {
        return resolve(mockDataContacts);
      }, 1000);
    } catch (e) {
      reject(e);
    }
  });
};

fetchData();

//test filtering and sorting with fetching mock data
export const useFetchContacts = () => {
  const [contacts, setContacts] = useState<ContactModel[]>(mockDataContacts);
  const [isFetching, setIsFetching] = useState<boolean>(false);

  const onSortContacts = async (params: any) => {
    setIsFetching(true);
    try {
      const data = await fetchData();
      setContacts([...data]);
    } catch (e) {
      throw new Error("onSortContacts: ", e as Error);
    } finally {
      setIsFetching(false);
    }
  };

  const onFilterContacts = (params: any) => {
    console.log("onFilterContacts:", params);
  };

  return { contacts, onFilterContacts, onSortContacts, isFetching };
};
