import { FC } from "react";
import { Box } from "@mui/material";

import { Header } from "../../components";

type Props = {
  title: string;
  subtitle: string;
  children: JSX.Element;
};
export const Page: FC<Props> = ({ title, subtitle, children }) => {
  return (
    <Box m="20px">
      <Header title={title} subtitle={subtitle} />
      {children}
    </Box>
  );
};
