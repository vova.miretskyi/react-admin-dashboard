import { PAGES_URLS } from "../routes";

export const findPageRouteByPath = (path: string) => {
  const adminUrlsList = Object.values(PAGES_URLS);

  const route = path
    .split("/")
    .filter((item) => !!item)
    .find((route) => adminUrlsList.some((url) => url.includes(route as any)));

  return route as keyof typeof PAGES_URLS;
};
