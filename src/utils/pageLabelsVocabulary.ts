export const PAGE_LABELS = {
  dashboard: "Dashboard",
  form: "Profile Form",
  team: "Manage Team",
  contacts: "Contacts Information",
  invoices: "Invoices Balances",
  bar: "Bar Chart",
  pie: "Pie Chart",
  line: "Line Chart",
  faq: "FAQ Page",
  geography: "Geography Chart",
  calendar: "Calendar",
} as const;
